-- mysql 8.0
-- 2020-04-20 15:00:00
-- Path: backend/scripts/000_create_db.sql

--  crear base de datos y usuario
CREATE DATABASE IF NOT EXISTS `Natura` DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci;
CREATE USER IF NOT EXISTS 'Natura'@'localhost' IDENTIFIED BY 'Natura';
GRANT ALL PRIVILEGES ON Natura.* TO 'Natura'@'localhost';
FLUSH PRIVILEGES;

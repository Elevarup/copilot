-- Usar la base de datos Natura
USE Natura;

-- insertar valores por defecto en la tabla de roles, si no existen
INSERT INTO roles (id, name) VALUES
(1, 'administrador'),
(2, 'usuario')
;
package config

import (
	"database/sql"
	"log"
	"os"
)

// variables para la configuración de la base de datos
var (
	// Host de la base de datos
	Host string
	// Port de la base de datos
	Port string
	// User de la base de datos
	User string
	// Password de la base de datos
	Password string
	// Database de la base de datos
	Database string
)

// variable para inicializar la base de datos
var (
	DB *sql.DB
)

func init() {
	// obtener los valores de las variables de entorno desde en .env
	Host = os.Getenv("DB_HOST")
	Port = os.Getenv("DB_PORT")
	User = os.Getenv("DB_USER")
	Password = os.Getenv("DB_PASSWORD")
	Database = os.Getenv("DB_DATABASE")

	var err error
	// crear la conexión a la base de datos usando el paquete sql de go además de validar el error
	DB, err = sql.Open("mysql", User+":"+Password+"@tcp("+Host+":"+Port+")/"+Database)
	// validar el error
	if err != nil {
		// generar el log fatal del error
		log.Fatal(err)
	}

}

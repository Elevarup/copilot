package user

import "gitlab.com/Elevarup/copilot/internal/pkg/pipes"

// Repositorio de usuarios para la conexión a la base de datos
type UserRepository interface {
	// Find retorna un usuario por su id
	Find(id int) (*pipes.User, error)
	// FindAll retorna todos los usuarios
	FindAll() ([]*pipes.User, error)
	// New crea un nuevo usuario
	New(user *pipes.User) error
	// Update actualiza un usuario
	Update(user *pipes.User) error
	// Delete elimina un usuario
	Delete(id int) error
	// FindByRol retorna todos los usuarios por su rol
	FindByRol(rol int) ([]*pipes.User, error)
}

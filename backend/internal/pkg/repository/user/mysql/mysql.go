package mysql

import (
	"context"
	"database/sql"
	"log"

	"gitlab.com/Elevarup/copilot/internal/pkg/config"
	"gitlab.com/Elevarup/copilot/internal/pkg/pipes"
)

// estructura para la implementación de la interfaz UserRepository
type (
	mysqlUserRepository struct{}
)

var (
	// variable para la conexión a la base de datos
	Conn *sql.Conn
)

// conectar a la base de datos
func conectarseDB() {
	var err error
	// crear la conexión a la base de datos usando el paquete sql de go además de validar el error
	Conn, err = config.DB.Conn(context.Background())
	// validar el error
	if err != nil {
		//generar el log del error
		log.Println(err)
	}
}

// cerrar la conexión a la base de datos
func Close() {
	// cerrar la conexión a la base de datos
	Conn.Close()
}

// Implementar el método Find de la interfaz UserRepository
func (m *mysqlUserRepository) Find(id int) (user *pipes.User, err error) {
	// conectar a la base de datos
	conectarseDB()
	// cerrar la conexion a la base de datos
	defer Close()

	// crear la consulta para obtener un users por su id
	query := "SELECT id, name, lastName, birthday, rol_id FROM users WHERE id = ?"

	// ejecutar la consulta y asignar el resultado a la variable user
	err = Conn.QueryRowContext(context.Background(), query, id).Scan(&user.ID, &user.FirstName, &user.LastName, &user.BirthDate, &user.RolID)
	// validar el error de la consulta y generar un log
	if err != nil {
		// generar un log del error
		log.Println(err)
	}

	return
}

// Implementar el método FindAll de la interfaz UserRepository
func (m *mysqlUserRepository) FindAll() (users []*pipes.User, err error) {
	// conectar a la base de datos
	conectarseDB()
	// cerrar la conexion a la base de datos
	defer Close()

	// crear la consulta para obtener todos los users
	query := "SELECT id, name, lastName, birthday, rol_id FROM users"

	// ejecutar la consulta y asignar el resultado a la variable users
	rows, err := Conn.QueryContext(context.Background(), query)
	// validar el error de la consulta y generar un log
	if err != nil {
		// generar un log del error
		log.Println(err)
	}

	// recorrer el resultado de la consulta
	for rows.Next() {
		// crear una variable para almacenar el resultado de la consulta
		var user pipes.User
		// asignar el resultado de la consulta a la variable user
		err = rows.Scan(&user.ID, &user.FirstName, &user.LastName, &user.BirthDate, &user.RolID)
		// validar el error de la consulta y generar un log
		if err != nil {
			// generar un log del error
			log.Println(err)
			continue
		}
		// agregar el resultado de la consulta a la variable users
		users = append(users, &user)
	}

	return
}

// Implementar el método New de la interfaz UserRepository
func (m *mysqlUserRepository) New(user *pipes.User) (err error) {
	// conectar a la base de datos
	conectarseDB()
	// cerrar la conexion a la base de datos
	defer Close()

	// crear la consulta para crear un nuevo users
	query := "INSERT INTO users (name, lastName, birthday, rol_id) VALUES (?, ?, ?, ?)"

	// ejecutar la consulta y asignar el resultado a la variable err
	_, err = Conn.ExecContext(context.Background(), query, user.FirstName, user.LastName, user.BirthDate, user.RolID)
	// validar el error de la consulta y generar un log
	if err != nil {
		// generar un log del error
		log.Println(err)
	}

	return
}

// Implementar el método Update de la interfaz UserRepository
func (m *mysqlUserRepository) Update(user *pipes.User) (err error) {
	// conectar a la base de datos
	conectarseDB()
	// cerrar la conexion a la base de datos
	defer Close()

	// crear la consulta para actualizar un users
	query := "UPDATE users SET name = ?, lastName = ?, birthday = ?, rol_id = ? WHERE id = ?"

	// ejecutar la consulta y asignar el resultado a la variable err
	_, err = Conn.ExecContext(context.Background(), query, user.FirstName, user.LastName, user.BirthDate, user.RolID, user.ID)
	// validar el error de la consulta y generar un log
	if err != nil {
		// generar un log del error
		log.Println(err)
	}

	return
}

// Implementar el método Delete de la interfaz UserRepository
func (m *mysqlUserRepository) Delete(id int) (err error) {
	// conectar a la base de datos
	conectarseDB()
	// cerrar la conexion a la base de datos
	defer Close()

	// crear la consulta para eliminar un users
	query := "DELETE FROM users WHERE id = ?"

	// ejecutar la consulta y asignar el resultado a la variable err
	_, err = Conn.ExecContext(context.Background(), query, id)
	// validar el error de la consulta y generar un log
	if err != nil {
		// generar un log del error
		log.Println(err)
	}

	return
}

// Implementar el método FindByRol de la interfaz UserRepository
func (m *mysqlUserRepository) FindByRol(rolID int) (users []*pipes.User, err error) {
	// conectar a la base de datos
	conectarseDB()
	// cerrar la conexion a la base de datos
	defer Close()

	// crear la consulta para obtener todos los users por rol
	query := "SELECT id, name, lastName, birthday, rol_id FROM users WHERE rol_id = ?"

	// ejecutar la consulta y asignar el resultado a la variable users
	rows, err := Conn.QueryContext(context.Background(), query, rolID)
	// validar el error de la consulta y generar un log
	if err != nil {
		// generar un log del error
		log.Println(err)
	}

	// recorrer el resultado de la consulta
	for rows.Next() {
		// crear una variable para almacenar el resultado de la consulta
		var user pipes.User
		// asignar el resultado de la consulta a la variable user
		err = rows.Scan(&user.ID, &user.FirstName, &user.LastName, &user.BirthDate, &user.RolID)
		// validar el error de la consulta y generar un log
		if err != nil {
			// generar un log del error
			log.Println(err)
			continue
		}
		// agregar el resultado de la consulta a la variable users
		users = append(users, &user)
	}

	return
}

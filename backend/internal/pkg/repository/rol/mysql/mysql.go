package mysql

import (
	"context"
	"database/sql"
	"log"

	"gitlab.com/Elevarup/copilot/internal/pkg/config"
	"gitlab.com/Elevarup/copilot/internal/pkg/pipes"
)

// estructura para el administrador de la base de datos mysql
type (
	mysqlRolRepository struct{}
)

var (
	Conn *sql.Conn
)

func conectarseDB() {
	var err error
	// crear la conexión a la base de datos usando el paquete sql de go además de validar el error
	Conn, err = config.DB.Conn(context.Background())
	// validar el error
	if err != nil {
		//generar el log del error
		log.Println(err)
	}
}

// generar un método para cerrar la conexión a la base de datos
func Close() {
	// cerrar la conexión a la base de datos
	Conn.Close()
}

// Implementar el método Find de la interfaz AdminRepository
func (m *mysqlRolRepository) Find(id int) (admin *pipes.Admin, err error) {
	// conectar a la base de datos
	conectarseDB()
	// cerrar la conexion a la base de datos
	defer Close()

	// crear la consulta para obtener un administrador por su id
	query := "SELECT id, name FROM admins WHERE id = ?"

	// ejecutar la consulta y asignar el resultado a la variable admin
	err = Conn.QueryRowContext(context.Background(), query, id).Scan(&admin.ID, &admin.Name)
	// validar el error de la consulta y generar un log
	if err != nil {
		// generar un log del error
		log.Println(err)
	}

	return
}

// Implementar el método FindAll de la interfaz AdminRepository
func (m *mysqlRolRepository) FindAll() (admins []*pipes.Admin, err error) {
	// conectar a la base de datos
	conectarseDB()
	// cerrar la conexion a la base de datos
	defer Close()

	// crear la consulta para obtener todos los administradores
	query := "SELECT id, name FROM admins"
	// ejecutar la consulta y asignar el resultado a la variable admins
	rows, err := Conn.QueryContext(context.Background(), query)
	// validar el error de la consulta y generar un log
	if err != nil {
		// generar un log del error
		log.Println(err)
	}
	// asignar el resultado de la consulta a la variable admins
	for rows.Next() {
		// crear una variable para el administrador
		var admin pipes.Admin
		// asignar el resultado de la consulta a la variable admin
		err = rows.Scan(&admin.ID, &admin.Name)
		// validar el error de la consulta y generar un log
		if err != nil {
			// generar un log del error
			log.Println(err)
			continue
		}
		// agregar el administrador a la lista de administradores
		admins = append(admins, &admin)
	}

	return
}

// Implementar el método New de la interfaz AdminRepository
func (m *mysqlRolRepository) New(admin *pipes.Admin) (err error) {
	// conectar a la base de datos
	conectarseDB()
	// cerrar la conexion a la base de datos
	defer Close()

	// crear la consulta para crear un nuevo administrador
	query := "INSERT INTO admins (name) VALUES (?)"
	// ejecutar la consulta y asignar el resultado a la variable admin
	_, err = Conn.ExecContext(context.Background(), query, admin.Name)
	// validar el error de la consulta y generar un log
	if err != nil {
		// generar un log del error
		log.Println(err)
	}
	return
}

// Implementar el método Update de la interfaz AdminRepository
func (m *mysqlRolRepository) Update(admin *pipes.Admin) (err error) {
	// conectar a la base de datos
	conectarseDB()
	// cerrar la conexion a la base de datos
	defer Close()

	// crear la consulta para actualizar un administrador
	query := "UPDATE admins SET name = ? WHERE id = ?"
	// ejecutar la consulta y asignar el resultado a la variable admin
	_, err = Conn.ExecContext(context.Background(), query, admin.Name, admin.ID)
	// validar el error de la consulta y generar un log
	if err != nil {
		// generar un log del error
		log.Println(err)
	}
	return
}

// Implementar el método Delete de la interfaz AdminRepository
func (m *mysqlRolRepository) Delete(id int) (err error) {
	// conectar a la base de datos
	conectarseDB()
	// cerrar la conexion a la base de datos
	defer Close()

	// crear la consulta para eliminar un administrador
	query := "DELETE FROM admins WHERE id = ?"
	// ejecutar la consulta y asignar el resultado a la variable admin
	_, err = Conn.ExecContext(context.Background(), query, id)
	// validar el error de la consulta y generar un log
	if err != nil {
		// generar un log del error
		log.Println(err)
	}

	return
}

package rol

import "gitlab.com/Elevarup/copilot/internal/pkg/pipes"

// Repositorio de roles para la conexión a la base de datos
type RolRepository interface {
	// Find retorna un administrador por su id
	Find(id int) (*pipes.Admin, error)
	// FindAll retorna todos los administradores
	FindAll() ([]*pipes.Admin, error)
	// New crea un nuevo administrador
	New(admin *pipes.Admin) error
	// Update actualiza un administrador
	Update(admin *pipes.Admin) error
	// Delete elimina un administrador
	Delete(id int) error
}

// variable para el repositorio de administradores
var adminRepository RolRepository

// NewAdminRepository crea un nuevo repositorio de administradores
func NewAdminRepository(r RolRepository) {
	adminRepository = r
}

// Find retorna un administrador por su id
func Find(id int) (*pipes.Admin, error) {
	return adminRepository.Find(id)
}

// FindAll retorna todos los administradores
func FindAll() ([]*pipes.Admin, error) {
	return adminRepository.FindAll()
}

// New crea un nuevo administrador
func New(admin *pipes.Admin) error {
	return adminRepository.New(admin)
}

// Update actualiza un administrador
func Update(admin *pipes.Admin) error {
	return adminRepository.Update(admin)
}

// Delete elimina un administrador
func Delete(id int) error {
	return adminRepository.Delete(id)
}

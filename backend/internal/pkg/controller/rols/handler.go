package rols

// importar librerías
import (
	"encoding/json"
	"log"
	"net/http"
	"regexp"
	"strconv"

	"gitlab.com/Elevarup/copilot/internal/pkg/pipes"
	rolRepository "gitlab.com/Elevarup/copilot/internal/pkg/repository/rol"
)

// estructura handler para la implementación de la interfaz RolHandler
type (
	Handler struct {
		// variable para la implementación de la interfaz RolRepository
		RolRepository rolRepository.RolRepository
	}
)

// método http.Handler para la interfaz RolHandler
func (h *Handler) ServeHTTP(w http.ResponseWriter, r *http.Request) {
	// validar el método de la petición
	switch r.Method {
	// si el método es GET
	case http.MethodGet:
		// validar la url
		switch r.URL.Path {
		// si la url es /rols
		case "/rols":
			// ejecutar el método FindAll de la interfaz RolHandler
			rols, err := h.RolRepository.FindAll()
			// validar el error
			if err != nil {
				// generar un log del error
				log.Println(err)
			}
			// convertir el resultado a json
			json.NewEncoder(w).Encode(rols)
		// si la url es /rols/{id}
		case regexp.MustCompile(`^/rols/\d+$`).FindString(r.URL.Path):
			// obtener el id de la url
			id, _ := strconv.Atoi(r.URL.Path[len("/rols/"):])
			// ejecutar el método Find de la interfaz RolHandler
			rol, err := h.RolRepository.Find(id)
			// validar el error
			if err != nil {
				// generar un log del error
				log.Println(err)
			}
			// convertir el resultado a json
			json.NewEncoder(w).Encode(rol)
		}
	// si el método es POST
	case http.MethodPost:
		// validar la url
		switch r.URL.Path {
		// si la url es /rols
		case "/rols":
			// crear una variable para el rol
			var rol pipes.Admin
			// decodificar el json del body
			json.NewDecoder(r.Body).Decode(&rol)
			// ejecutar el método New de la interfaz RolHandler
			err := h.RolRepository.New(&rol)
			// validar el error
			if err != nil {
				// generar un log del error
				log.Println(err)
			}
			// convertir el resultado a json
			json.NewEncoder(w).Encode(rol)
		}
	// si el método es PUT
	case http.MethodPut:
		// validar la url
		switch r.URL.Path {
		// si la url es /rols/{id}
		case regexp.MustCompile(`^/rols/\d+$`).FindString(r.URL.Path):
			// obtener el id de la url
			id, _ := strconv.Atoi(r.URL.Path[len("/rols/"):])
			// crear una variable para el rol
			var rol pipes.Admin
			// asignar el id al rol
			rol.ID = id
			// decodificar el json del body
			json.NewDecoder(r.Body).Decode(&rol)
			// ejecutar el método Update de la interfaz RolHandler
			err := h.RolRepository.Update(&rol)
			// validar el error
			if err != nil {
				// generar un log del error
				log.Println(err)
			}
			// convertir el resultado a json
			json.NewEncoder(w).Encode("Rol actualizado")
		}
	// si el método es DELETE
	case http.MethodDelete:
		// validar la url
		switch r.URL.Path {
		// si la url es /rols/{id}
		case regexp.MustCompile(`^/rols/\d+$`).FindString(r.URL.Path):
			// obtener el id de la url
			id, _ := strconv.Atoi(r.URL.Path[len("/rols/"):])
			// ejecutar el método Delete de la interfaz RolHandler
			err := h.RolRepository.Delete(id)
			// validar el error
			if err != nil {
				// generar un log del error
				log.Println(err)
			}
			// convertir el resultado a json
			json.NewEncoder(w).Encode("Rol eliminado")
		}
	}
}

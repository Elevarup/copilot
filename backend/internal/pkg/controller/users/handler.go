package users

import (
	"encoding/json"
	"log"
	"net/http"
	"regexp"
	"strconv"

	"gitlab.com/Elevarup/copilot/internal/pkg/pipes"
	userRepository "gitlab.com/Elevarup/copilot/internal/pkg/repository/user"
)

// estructura handler para la implementación de la interfaz UserHandler
type (
	Handler struct {
		// variable para la implementación de la interfaz UserRepository
		UserRepository userRepository.UserRepository
	}
)

// Implementar http.Handler para la interfaz UserHandler
func (h *Handler) ServeHTTP(w http.ResponseWriter, r *http.Request) {
	// validar el método de la petición
	switch r.Method {
	// si el método es GET
	case http.MethodGet:
		// validar la url
		switch r.URL.Path {
		// si la url es /users
		case "/users":
			// ejecutar el método FindAll de la interfaz UserHandler
			users, err := h.UserRepository.FindAll()
			// validar el error
			if err != nil {
				// generar un log del error
				log.Println(err)
			}
			// convertir el resultado a json
			json.NewEncoder(w).Encode(users)
		// si la url es /users/{id}
		case regexp.MustCompile(`^/users/\d+$`).FindString(r.URL.Path):
			// obtener el id de la url
			id, _ := strconv.Atoi(r.URL.Path[len("/users/"):])
			// ejecutar el método Find de la interfaz UserHandler
			user, err := h.UserRepository.Find(id)
			// validar el error
			if err != nil {
				// generar un log del error
				log.Println(err)
			}
			// convertir el resultado a json
			json.NewEncoder(w).Encode(user)
		}
	// si el método es POST
	case http.MethodPost:
		// validar la url
		switch r.URL.Path {
		// si la url es /users
		case "/users":
			// crear una variable para el usuario
			var user pipes.User
			// decodificar el json de la petición
			json.NewDecoder(r.Body).Decode(&user)
			// ejecutar el método New de la interfaz UserRepository
			err := h.UserRepository.New(&user)
			// validar el error
			if err != nil {
				// generar un log del error
				log.Println(err)
			}
			// convertir el resultado a json
			json.NewEncoder(w).Encode(user)
		}
	// si el método es PUT
	case http.MethodPut:
		// validar la url
		switch r.URL.Path {
		// si la url es /users/{id}
		case regexp.MustCompile(`^/users/\d+$`).FindString(r.URL.Path):
			// obtener el id de la url
			id, _ := strconv.Atoi(r.URL.Path[len("/users/"):])
			// crear una variable para el usuario
			var user pipes.User
			// asignar el id al usuario
			user.ID = id
			// decodificar el json de la petición
			json.NewDecoder(r.Body).Decode(&user)
			// ejecutar el método Update de la interfaz UserRepository
			err := h.UserRepository.Update(&user)
			// validar el error
			if err != nil {
				// generar un log del error
				log.Println(err)
			}
			// convertir el resultado a json
			json.NewEncoder(w).Encode(user)
		}
	// si el método es DELETE
	case http.MethodDelete:
		// validar la url
		switch r.URL.Path {
		// si la url es /users/{id}
		case regexp.MustCompile(`^/users/\d+$`).FindString(r.URL.Path):
			// obtener el id de la url
			id, _ := strconv.Atoi(r.URL.Path[len("/users/"):])
			// ejecutar el método Delete de la interfaz UserRepository
			err := h.UserRepository.Delete(id)
			// validar el error
			if err != nil {
				// generar un log del error
				log.Println(err)
			}
			// convertir el resultado a json
			json.NewEncoder(w).Encode("ok")
		}
	// si el método no es soportado
	default:
		// generar un log del error
		log.Println("method not supported")
		// convertir el resultado a json
		json.NewEncoder(w).Encode("method not supported")
	}
}

package pipes

import "time"

// User es el modelo de usuario que se relaciona con la tabla users de la base de datos
type User struct {
	ID        int       `json:"id"`
	FirstName string    `json:"first_name"`
	LastName  string    `json:"last_name"`
	BirthDate time.Time `json:"birthday"`
	RolID     int       `json:"rol_id"`
}

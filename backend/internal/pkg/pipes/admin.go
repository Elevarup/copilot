package pipes

// Admin es la estructura de un administrador de la base de datos
type Admin struct {
	// ID es el identificador del administrador
	ID int `json:"id"`
	// Name es el nombre del administrador
	Name string `json:"name"`
}

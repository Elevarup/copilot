// validar el nombre, el apellido y la fecha de nacimiento
function validacion(){
    // validar nombre
    let nombre = document.getElementById("nombre").value;
    // validar apellido
    let apellido = document.getElementById("apellido").value;
    // validar fecha de nacimiento
    let fechaNacimiento = document.getElementById("fechaNacimiento").value;
    // obtener el rol del usuario
    let rol = document.getElementById("rol").value;
    // validar que el nombre no este vacio y que no tenga numeros
    if(nombre == null || nombre.length == 0 || /^\s+$/.test(nombre) || !isNaN(nombre)){
        alert("El nombre no es valido");
        return;
    }
    // validar que el apellido no este vacio y que no tenga numeros
    if(apellido == null || apellido.length == 0 || /^\s+$/.test(apellido) || !isNaN(apellido)){
        alert("El apellido no es valido");
        return;
    }
    // validar que la fecha de nacimiento no este vacia
    if(fechaNacimiento == null || fechaNacimiento.length == 0){
        alert("La fecha de nacimiento no es valida");
        return;
    }
    // validar que la fecha de nacimiento sea menor a la fecha actual
    let fechaActual = new Date();
    let fechaNacimientoDate = new Date(fechaNacimiento);
    if(fechaNacimientoDate > fechaActual){
        alert("La fecha de nacimiento no es valida");
        return;
    }
    // validar que el rol no este vacio
    if(rol == null || rol.length == 0){
        alert("El rol no es valido");
        return;
    }
}